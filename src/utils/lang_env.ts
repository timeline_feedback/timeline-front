import { LANG } from "/src/utils/env";

const get_lang_in_array = (): string[] => {
    let cur_lang = LANG;
    if (!cur_lang || typeof cur_lang === 'boolean') return [];
    while (cur_lang.endsWith(',')) cur_lang = cur_lang.slice(0, cur_lang.length -1);
    while (cur_lang.startsWith(',')) cur_lang = cur_lang.slice(1, cur_lang.length);
    return cur_lang.split(',').filter(lang => typeof lang === 'string' && lang).map(lang => lang.toLocaleLowerCase());
}

export const app_langs = get_lang_in_array();
export const DEFAUTL_LANG = app_langs[0];