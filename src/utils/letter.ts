// TODO: Add Test
export const make_first_letter_uppercase = (str: string): string => {
    return `${str.slice(0,1).toLocaleUpperCase()}${str.slice(1)}`;
}