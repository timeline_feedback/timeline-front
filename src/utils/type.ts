export type func_callback_click = (
	e: MouseEvent & { currentTarget: EventTarget & HTMLButtonElement }
) => boolean | void;

export enum Checkpoint {
	START = 'START',
	SUCCESS = 'SUCCESS',
	FAILURE = 'FAILURE',
	SIMPLE = 'SIMPLE',
	TARGET = 'TARGET'
}

interface IArticlesSlug {
	[key: string]: string;
}

interface ILinks {
	[key: string]: string;
}

interface IProject {
	id: string;
	name: string;
	links: ILinks;
}

export interface IArticle {
	id: string | number;
	start_time: number;
	title: string;
	article_slug: string;
	lang: string;
	articles_slug: IArticlesSlug;
	langs: string[];
	project: IProject;
	abstract: string;
	article: string;
}
