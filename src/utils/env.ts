export const LANG = import.meta.env.VITE_LANG;
export const API_URL = import.meta.env.VITE_API_URL;
export const API_KEY = import.meta.env.VITE_API_KEY;
export const API_KEY_QUERY_NAME = import.meta.env.VITE_API_KEY_QUERY_NAME;